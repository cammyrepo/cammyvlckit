Pod::Spec.new do |s|
  s.name             = "CammyVLCKit"
  s.version          = "2.2.0"
  s.summary          = "Pod for working with compiled MobileVLCKit."

  s.platform     = :ios, '7.0'
  s.source = { :git => "https://bitbucket.org/cammyrepo/cammyvlckit.git", :tag => s.version.to_s }
  s.requires_arc = true

  s.preserve_paths = 'MobileVLCKit.framework'
  s.vendored_frameworks = 'MobileVLCKit.framework'
  s.libraries = 'bz2', 'iconv', 'stdc++'

  s.frameworks = 'AudioToolbox', 'CoreAudio', 'OpenGLES', 'CFNetwork', 'CoreText', 'QuartzCore', 'CoreGraphics', 'UIKit'

end